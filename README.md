# README

This project is used internally by the design team for issue tracking. Each issue is used as a meta tracker for a design initiative, which is loosely defined as a non-trivial design task that we want to have some long-term awareness of.

The main way to use the initiatives tracker is through the [issue board](https://gitlab.gnome.org/Teams/Design/initiatives/-/boards).

Each issue in the tracker should only contain:

* a description of the design task or initiative
* links to one or more issues in other trackers (which is where the real design work should happen)
* comments about the status of the initiative

The issues in this tracker should not contain design discussions or detailed proposals.